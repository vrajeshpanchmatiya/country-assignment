import axios from "axios";
// countryService for calling api
export const countryService = (name) => {
  return axios.get(`${process.env.REACT_APP_API_URL_COUNTRY}${name}`);
};
