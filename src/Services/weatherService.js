import axios from "axios";
// weatherService for  calling api
export const weatherService = (name) => {
  return axios.get(`${process.env.REACT_APP_API_URL_WEATHER}${name}`);
};
