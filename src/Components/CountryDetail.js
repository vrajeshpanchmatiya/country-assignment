import { Avatar, Box, Button, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { weatherAction } from "../Actions/weatherAction";
import "../Common.scss";
const CountryDetail = () => {
  const dispatch = useDispatch();
  // useSelector for fetching data from api
  const detail = useSelector((state) => {
    return state.country.data;
  });
  // dispatch capital name on click check weather
  const weather = () => {
    dispatch(weatherAction(detail.capital));
  };
  // useSelector for fetching data from api
  const info = useSelector((state) => {
    return state.weather.data;
  });

  return (
    <div className="di">
      <Box className="bx1">
        <Box className="bx2">
          <h3>Country Detail</h3>
          <Typography>
            <b>Capital: </b>
            {detail.capital}
          </Typography>
          <Typography>
            <b>Population: </b>
            {detail.population}
          </Typography>
          {detail?.latlng && Array.isArray(detail.latlng)
            ? detail.latlng.map((latlng) => {
                return (
                  <Typography>
                    <b>Latlng: </b>
                    {latlng}
                  </Typography>
                );
              })
            : null}
          <Avatar src={detail?.flag} />
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            onClick={weather}
          >
            Check Weather
          </Button>
        </Box>
        <Box className="bx3">
          <h3>Weather Detail</h3>
          <Typography>
            <b>Temperature: </b>
            {info.temperature}
          </Typography>
          <Typography>
            <b>Wind Speed: </b>
            {info.wind_speed}
          </Typography>
          <Typography>
            <b>Precip: </b>
            {info.precip}
          </Typography>
          {info?.weather_icons && Array.isArray(info.weather_icons)
            ? info.weather_icons.map((icon) => {
                return <Avatar src={icon} />;
              })
            : null}
        </Box>
      </Box>
    </div>
  );
};
export default CountryDetail;
