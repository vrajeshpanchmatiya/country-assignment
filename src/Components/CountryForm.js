import { Box, Button, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { countryAction } from "../Actions/countryAction";
import "../Common.scss";
const CountryForm = () => {
  const [name, setName] = useState(null);
  const dispatch = useDispatch();
  // changeName for change event handler
  const changeName = (e) => {
    setName(e.target.value);
  };
  // dispatch capital name on click handleCountryName
  const handleCountryName = () => {
    dispatch(countryAction(name));
  };
  // Country Form for fetch Country Detail
  return (
    <div className="di">
      <Box className="bx">
        <h1>Country Form</h1>
        <TextField
          name={name}
          label="Country Name"
          variant="outlined"
          color="primary"
          onChange={changeName}
        />
        <Link
          to={{ pathname: "/CountryDetail" }}
          style={{ textDecoration: "none" }}
        >
          <Button
            type="submit"
            onClick={handleCountryName}
            variant="outlined"
            color="primary"
            disabled={name === null || name.length <= 1}
          >
            Submit Country Name
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default CountryForm;
