import { weatherType } from "./Type/weatherType";
import { weatherService } from "../Services/weatherService";
export const weatherAction = (name) => {
  return async (dispatch) => {
    const detail = await weatherService(name);
    console.log(detail.data);
    dispatch({ type: weatherType, payload: detail.data.current });
  };
};
