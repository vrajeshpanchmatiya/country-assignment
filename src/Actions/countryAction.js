import { countryType } from "./Type/countryType";
import { countryService } from "../Services/countryService";
export const countryAction = (name) => {
  return async (dispatch) => {
    const detail = await countryService(name);
    dispatch({ type: countryType, payload: detail.data[0] });
  };
};
