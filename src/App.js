import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import CountryForm from "./Components/CountryForm";
import CountryDetail from "./Components/CountryDetail";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={CountryForm} />
          <Route path="/CountryDetail" component={CountryDetail} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
