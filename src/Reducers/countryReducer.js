import { countryType } from "../Actions/Type/countryType";
// initial value of state
const initialState = {
  data: [],
};
// Reducer for store store
export const countryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case countryType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
